msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-03 00:41+0000\n"
"PO-Revision-Date: 2019-12-24 22:22-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr ""

#: deviceindicator.cpp:67
#, kde-format
msgid "Send clipboard"
msgstr ""

#: deviceindicator.cpp:81
#, kde-format
msgctxt "@action:inmenu play bell sound"
msgid "Ring device"
msgstr ""

#: deviceindicator.cpp:97
#, kde-format
msgid "Send a file/URL"
msgstr ""

#: deviceindicator.cpp:107
#, kde-format
msgid "SMS Messages..."
msgstr ""

#: deviceindicator.cpp:120
#, kde-format
msgid "Run command"
msgstr ""

#: deviceindicator.cpp:122
#, kde-format
msgid "Add commands"
msgstr ""

#: indicatorhelper_mac.cpp:37
#, kde-format
msgid "Launching"
msgstr ""

#: indicatorhelper_mac.cpp:87
#, kde-format
msgid "Launching daemon"
msgstr ""

#: indicatorhelper_mac.cpp:96
#, kde-format
msgid "Waiting D-Bus"
msgstr ""

#: indicatorhelper_mac.cpp:113 indicatorhelper_mac.cpp:125
#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "KDE Connect"
msgstr ""

#: indicatorhelper_mac.cpp:114 indicatorhelper_mac.cpp:126
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""

#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr ""

#: indicatorhelper_mac.cpp:149
#, kde-format
msgid "Loading modules"
msgstr ""

#: main.cpp:53
#, kde-format
msgid "KDE Connect Indicator"
msgstr ""

#: main.cpp:55
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr ""

#: main.cpp:57
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr ""

#: main.cpp:92
#, kde-format
msgid "Configure..."
msgstr ""

#: main.cpp:114
#, kde-format
msgid "Pairing requests"
msgstr ""

#: main.cpp:119
#, kde-format
msgctxt "Accept a pairing request"
msgid "Pair"
msgstr ""

#: main.cpp:120
#, kde-format
msgid "Reject"
msgstr ""

#: main.cpp:126 main.cpp:136
#, kde-format
msgid "Quit"
msgstr ""

#: main.cpp:155 main.cpp:179
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] ""

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr ""

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr ""

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr ""

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr ""

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr ""
