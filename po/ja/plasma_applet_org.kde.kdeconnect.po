# <tomhioo@outlook.jp>, 2019.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022, 2023.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.kdeconnect\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-22 00:41+0000\n"
"PO-Revision-Date: 2023-04-07 23:39-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1% charging"
msgstr "%1% 充電中"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "No info"
msgstr "情報なし"

#: package/contents/ui/Connectivity.qml:40
#, kde-format
msgid "Unknown"
msgstr "不明"

#: package/contents/ui/Connectivity.qml:50
#, kde-format
msgid "No signal"
msgstr "シグナルなし"

#: package/contents/ui/DeviceDelegate.qml:56
#, kde-format
msgid "File Transfer"
msgstr "ファイル転送"

#: package/contents/ui/DeviceDelegate.qml:57
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "ファイルをドロップして端末に転送"

#: package/contents/ui/DeviceDelegate.qml:93
#, kde-format
msgid "Virtual Display"
msgstr "仮想ディスプレイ"

#: package/contents/ui/DeviceDelegate.qml:146
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:168
#, kde-format
msgid "Please choose a file"
msgstr "ファイルを選択してください"

#: package/contents/ui/DeviceDelegate.qml:177
#, kde-format
msgid "Share file"
msgstr "ファイルを共有"

#: package/contents/ui/DeviceDelegate.qml:192
#, kde-format
msgid "Send Clipboard"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:211
#, kde-format
msgid "Ring my phone"
msgstr "端末を鳴らす"

#: package/contents/ui/DeviceDelegate.qml:229
#, kde-format
msgid "Browse this device"
msgstr "このデバイスを閲覧"

#: package/contents/ui/DeviceDelegate.qml:246
#, kde-format
msgid "SMS Messages"
msgstr "SMS メッセージ"

#: package/contents/ui/DeviceDelegate.qml:267
#, kde-format
msgid "Remote Keyboard"
msgstr "リモートキーボード"

#: package/contents/ui/DeviceDelegate.qml:288
#, kde-format
msgid "Notifications:"
msgstr "通知:"

#: package/contents/ui/DeviceDelegate.qml:296
#, kde-format
msgid "Dismiss all notifications"
msgstr "すべての通知を消去"

#: package/contents/ui/DeviceDelegate.qml:343
#, kde-format
msgid "Reply"
msgstr "返信"

#: package/contents/ui/DeviceDelegate.qml:353
#, kde-format
msgid "Dismiss"
msgstr "無視"

#: package/contents/ui/DeviceDelegate.qml:366
#, kde-format
msgid "Cancel"
msgstr "キャンセル"

#: package/contents/ui/DeviceDelegate.qml:380
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "%1に返信…"

#: package/contents/ui/DeviceDelegate.qml:398
#, kde-format
msgid "Send"
msgstr "送信"

#: package/contents/ui/DeviceDelegate.qml:424
#, kde-format
msgid "Run command"
msgstr "コマンドを実行"

#: package/contents/ui/DeviceDelegate.qml:432
#, kde-format
msgid "Add command"
msgstr "コマンドを追加"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "No paired devices"
msgstr "ペアリング済みのデバイスはありません"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "ペアリング済みのデバイスが利用できません"

#: package/contents/ui/FullRepresentation.qml:60
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"KDE Connect をあなたの Android デバイスにインストールして Plasma と統合しま"
"しょう!"

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Pair a Device..."
msgstr "デバイスとペアリングする..."

#: package/contents/ui/FullRepresentation.qml:76
#, kde-format
msgid "Install from Google Play"
msgstr "Google Play からインストール"

#: package/contents/ui/FullRepresentation.qml:86
#, kde-format
msgid "Install from F-Droid"
msgstr "F-Droid からインストール"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "KDE Connect Settings..."
msgstr "KDE Connect の設定..."
